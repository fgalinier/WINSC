# What is WINSC?

WINSC is a framework that wants to be simple, easy to handle, without big pretension.
The base idea is to have a basic MVC approach, where users can easily add plugins that will realize the common needed for a basic website.

This framework was, in a first time, developed by [Florian Galinier](https://gitlab.com/Ynigvi) in order to create his personal website and to explore web framework structure in the same time.
It is very inspired from [Symfony](https://symfony.com/) framework in its firsts versions, but will evolve to something less powerful, but easier to handle to non-web expert.

In the final version, an effort will be done to provide an efficient workspace.
The idea is to use MDE concepts to allow a model driven development.

# How to use it?

Due to the fact that the project is in an early version, most of the documentation is absent and will change. Here is describe in a nutshell how you can create part of your website. A more complete documentation will coming soon!

## Website project architecture

When you create your website, you have to focus on three folders:
- **src**,
- **config**,
- and **resources**.

The **src** directory will contains *modules*, that will be parts of your website. Each *module* is decomposed as follow:
- the view, in **src/ModuleName/resources** folder,
- the model, in **src/ModuleName/Object** folder,
- and controllers, in **src/ModuleName/Controller** folder, that make the glue between view and model.

In order to access to a module, you should configure a path. To do this, in **config** directory you have a **routes.json** file, organize as follow:
```JSON
{
    "<identifier for a route>" : {
    	"pattern": "<url path to access to the module (could be a regex)>",
        "controller": "<path to the controller>",
        "action": "<method of the controller to call>",
        "default": "<default url path>"
    }
}

```

The last folder, **resources**, is used to centralized common resources used by several *modules* (such as your website header).

The template system allows to compose different template parts (see [Template part](#template)).

## Create a new module for my website

In order to create a new component (let say the basic one, *Home* component that will be your website showcase), you juste have to create a new directory, named *Home*, in the **src** folder.

In this directory, you can create a basic controller (in the **src/Home/Controller** directory), in *PHP*. A basic template of the controller could be:
```PHP
<?php

namespace Home\Controller;
use Framework\Base\Controller;;
use Framework\Template\View;
use Home\Object\Article;

class HomeController extends Controller {
	public function indexAction() {
		return View::generate('src/Home/resources/views/index.html',array());
	}
}
```
The `indexAction()` is the method that you should precise in **routes.json** file. Here, it just call the template engine to generate a page, based on the **src/Home/resources/views/index.html** view, with no information (empty array `array()`).

You could send information to template engine through an associative array. For example, if you want to send a *name* that you will access in the view, you should pass in second argument `array('name' => 'WINSC')`. You could then access it in view (see [Template part](#template)).

## Template

After creating your controller, you can create a view that will be called by this controller. For the *Home* example, the views will be in **src/Home/resources/views/**. These views are *HTML* files, with a dedicated specific language (DSL) to abstract data (such as the name that you passed from the controller).

Here is an example of a simple view:
```HTML
		<% include resources/views/header.html %>
		<div class="main">
			This is the main page of your website, accessed through the HomeController.
		</div>
		<% include resources/views/footer.html %>
```

The DSL parts are braced between `<%` and `%>`. You could find below a description of principle DSL mechanism.

### Data access

When you pass data from the controller to a view, the simple way to access it is through its name. For example, for the *name* that you previously passed, you just have to write:
```HTML
<% name %>
```

If it is a complex data (a class or an array for example), you could access to field through the dot:
```HTML
<% name.field %>
```

Some operations can be executed on this data but are not developed here.

### Includes

As see in the first template example, you could include part of templates, declared in an other file.
You just have to use the `include` function with the good path into the place that you want to include it.

```HTML
		<% include resources/views/header.html %>
		<div class="main">
			This is the main page of your website, accessed through the HomeController.
		</div>
		<% include resources/views/footer.html %>
```
Here, the header (*resources/views/header.html*) and the footer (*resources/views/header.html*) are common resources, located in **resources** folder and are included in this page, in beginning and end of file.

### Conditions

You could conditionally write data. For example, if you want to write data only if this data is different to a value, you can do this with:
```HTML
		<% if not equals(data,value) %>
		<p><% data %></p>
		<% endif %>
```

### Loop

If you should iterate through an array, you can do it by using the `foreach` structure. The syntax is quite simple; for example, if you have an array of `article` named `articles`, you can write:
```HTML
	<% foreach article in articles %>
	<article>
		<h3 id="<% article.alias %>"><% article.title %></h3>
		<% article.content %>
	</article>
	<% endfor %>
```

# WINSC architecture

If you want to know how to develop with the help of WINSC framework, please, look at [Website project architecture](#website-project-architecture). Here is describe the architecture of framework for people who want to contribute.

This section just overfly the architecture, and should be taken as an entry point to the system. If you have any question, please ask.

Extra to folders described in [Website project architecture](#website-project-architecture), there is an other folder, named **app**, that contain core of the framework.

For now, there is three directories in this one:
- **Base**, that contains ~Core of the application;
- **ORM**, that contains the glue between *Objects* and database - the ~ORM system;
- and **Template**, that contains the ~Template engine.

The **router** into the ~Core is in charge to call the good controller when a page is asked.
These controllers are calling the template engine (**View** class) that will generate page, and used objects of the model, that inherit **Object** class, witch is managed by the ~ORM.

