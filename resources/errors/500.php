		<% include resources/views/header.html %>
		<div class="main">
			<section class="error">
				<h2>Server error</h2>
				<p>
					<span class="error-code"><% code %></span>
					An unexpected error occured. Please, contact administrator and join this message:
				</p>
				<p>
					<?php echo $message; ?>
				</p>
			</section>
		</div>
		<% include resources/views/footer.html %>
