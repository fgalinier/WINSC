		<% include resources/views/header.html %>
		<div class="main">
			<section class="error">
				<h2>Page not found</h2>
				<p>
					<span class="error-code"><% code %></span>
					Sorry, this page is not available.
				</p>
				<p>
					<a href="index.html">Return to index.</a>.
				</p>
			</section>
		</div>
		<% include resources/views/footer.html %>
