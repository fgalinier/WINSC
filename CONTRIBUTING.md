# How to contribute?

Firstly, thanks for your interest in our project. There are many way to contribute, and you can choose the one that is the more adapted for your situation.

## I see a bug/feature that is not listed, can I add it?

Of course! You can propose a new feature in the [issues tab](https://gitlab.com/Ynigvi/WINSC/issues).
Be sure to follow the [template](#how-to-format-issues) in order to ease the acceptance process.

## I want to contribute but I'm not part of the project. Can I?

Yes, you can fork the project and implement an accepted feature or fix a bug listed in the [*Issues* tab](https://gitlab.com/Ynigvi/WINSC/issues).
You can then ask for a merge request, that could be accepted after examination.
After all, look at the [workflow process](#workflow-process) in order to submit it in the right way.

## And if I'm part of the project?

Just follow the [workflow process](#workflow-process).

# How to format issues?

## Report a bug

When you report a bug, a good practice is to describe the problem itself, but also when and how is occured.
You can (and you are encourage to) submit a little code example to describe it.

If you now where is the problem, attach the right label to your issue. If you are not sure, it is ok too, you can let it blank, a developper will modify it.

## Submit a new feature

The feature in this project are formatted as user stories.
The idea is to describe a feature in this way:
```
As an <actor>, I should/would <action>.
```

This lead to identify which part of system is concerned.

When you submit a new feature, the feature will be labeled as ~"Feature (request)". After discussion (especially on "Is this feature a real requirement?"), the feature can become ~"Feature (rejected)" or just ~Feature if it is accepted.

If your feature is rejected, you could continue to debate on the necessity of the feature.

# Workflow process

In our workflow, we are focussing on one invariant: master branch should be functionnal.
In order to preserve this invariant, when you are developping a new feature, you have to create a new branch.
The name of the branch should start with `feature/` or `bug/` (depending if you are working on a feature or on a bug), followed by an explicit but concise name (*feature/plugins-system* is ok, but not *plugin-system*, *feature/add-a-plugin-system-to-WINSC-framework* or *feature/feat1*).

When you are writing code, be sure to respect the architecture, to write sufficient comment and to respect naming convention. If not, your branch could be rejected, even if working.

When you have finished to develop a feature, make a final commit with "Fix #<issue number>".

## Commits

When commiting, please be sure that your commit:
- start with the part of system that is modify (actually, that should be ~Core, ~Documentation, ~Framework, ~ORM, ~Plugins or ~Template);
- following by a few word description;
- and, if needed, in a new line, by a more descriptive text that describe what you've done.

A good commit should be:
```
Documentation: Add contribution guide.

I add a first draft of contribution guide. Should contains typo, but will be fixed later in order to provide a first framework.
```
Or:
```
Documentation: Fix #4

```
