<?php

namespace Framework\ORM;

class ORM {
	private $connection = null;
	
	public function __construct() {
		$this->connection = new Connection;
	}
	
	public function getConnection() {
		return $this->connection;
	}
	
	public function create($obj) {
		$class = new \ReflectionClass($obj);
		if(!$class->isSubclassOf('Framework\ORM\Object')) throw new \Exception;
		$properties = array_reverse($class->getProperties());
		$create_prop = array();
		$create_options = '';
		foreach($properties as $property) {
			$type;
			if(!preg_match('#@type\((.*)\)#i',$property->getDocComment(),$type))
				if(!preg_match('#@ref\((.*)\)#i',$property->getDocComment(),$type))	throw new \Exception;
				else {
					$create_options .= ('foreign key ('.$property->getName().') references '.$type[1].'(id)');
					$type[1] = 'int';
				}
			
			$opt = $type[1];
			preg_match_all('#@ORM\((.*)\)#i',$property->getDocComment(),$options);
			foreach($options[1] as $o) {
				$opt .= (' '.$o);
			}
			$create_prop[$property->getName()] = $opt;
		}
		return $this->connection->create($obj,$create_prop,$create_options)->exec();		
	}
	
	public function delete(Object $obj) {
		return $this->connection->delete(get_class($obj))->where(array('id = '.$obj->id))->exec();
	}
	
	public function add(Object $obj) {
		return $this->connection->insert(get_class($obj),$obj->getProperties())->exec();
	}
	
	public function update(Object $obj) {
		return $this->connection->update(get_class($obj),$obj->getProperties(),array('id = '.$obj->id))->exec();
	}
	
	public function maxId($obj) {
		return $this->connection->max('id')->from($obj)->exec()->fetch()[0];
	}
	
	public function count($obj, array $criteria = null) {
		return $this->connection->count()->from($obj)->where($criteria)->exec()->fetch()[0];
	}
	
	public function find($obj, array $criteria = null, array $order = null, $desc = false, $lim = null, $offset = null) {
		$class = new \ReflectionClass($obj);
		if(!$class->isSubclassOf('Framework\ORM\Object')) throw new \Exception;
		$res = $this->connection->select()->from($obj)->where($criteria)->order($order, $desc)->limit($lim, $offset)->exec();
		$ret = array();
		if(!$res) return $ret;
		$properties = $class->getProperties();
		while($o = $res->fetch()) {
			$n = new $obj;
			foreach($properties as $prop) {
				$name = $prop->getName();
				if(preg_match('#@ref\((.*)\)#i',$prop->getDocComment(),$matches))
					$n->$name = $this->findById($matches[1], $o[$prop->getName()]);
				else
					$n->$name = $o[$prop->getName()];
			}
			$n->isNew(false);
			$ret[] = $n;
		}
		return $ret;
	}
	
	public function findById($obj, $id) {
		$class = new \ReflectionClass($obj);
		if(!$class->isSubclassOf('Framework\ORM\Object')) throw new \Exception;
		$res = $this->connection->select()->from($obj)->where(array('id = '.$id))->exec();
		if(!$res) return null;
		$properties = $class->getProperties();
		$o = $res->fetch();
		
		$n = new $obj;
		foreach($properties as $prop) {
			$name = $prop->getName();
			if(preg_match('#@ref\((.*)\)#i',$prop->getDocComment(),$matches))
				$n->$name = $this->findById($matches[1], $o[$prop->getName()]);
			else
				$n->$name = $o[$prop->getName()];
		}
		$n->isNew(false);
		
		return $n;
	}
}
