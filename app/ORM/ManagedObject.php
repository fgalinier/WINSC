<?php

namespace Framework\ORM;

/**
  * Classes that extends ManagedObject should be implemented with two conditions :
  *		- if a property is public, it will be recorded in database. Type have to
  *		  be specified by @type() annotation.
  *		- if a constructor is implemented, it should call parent::__construct()
  *		  in order to avoid malfunctions.
  */
abstract class ManagedObject extends Object {
	
	public function save() {
		$o = parent::save();
		if($o) return $o;
		else {
			$o = static::createTable();
			if(!$o) return $o;
		}
		return parent::save();
	}
}
