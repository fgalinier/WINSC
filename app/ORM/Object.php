<?php

namespace Framework\ORM;

/**
  * Classes that extends Object should be implemented with two conditions :
  *		- if a property is public, it will be recorded in database. Type have to
  *		  be specified by @type() annotation.
  *		- if a constructor is implemented, it should call parent::__construct()
  *		  in order to avoid malfunctions.
  */
abstract class Object {
	private static $ORM = null;
	private $isNew;
	
	/**
	  * @type(int)
	  * @ORM(not null auto_increment)
	  */
	public $id;
	
	public function __construct() {
		if(!Object::$ORM) Object::$ORM = new ORM;
		$this->isNew = true;
	}
	
	public function load() { // Sers pour l'instant pour les tests
		try {
			$o = new \systeme;
			//if(\systeme::createTable()) echo 'true';
			//else echo 'false';
			if($o->save()) echo "true";
			else echo 'false';
		}
		catch(Exception $e) {
			die();
		}
	}
	
	public static function createTable() {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->create(static::class);
	}
	
	public function isNew($new = null) {
		if($new !== null) 
			$this->isNew = $new;			
		return $this->isNew;
	}
	
	public static function getInstances(array $criteria = null, array $order = null, $desc = false, $lim = null, $offset = null) {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->find(static::class,$criteria,$order,$desc,$lim,$offset);
	}
	
	public static function getInstanceById($id) {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->findById(static::class,$id);
	}
	
	public static function count(array $criteria = null) {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->count(static::class,$criteria);
	}
	
	public static function newId() {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->maxId(static::class)+1;
	}
	
	public static function delete(Object $obj) {
		if(!Object::$ORM) Object::$ORM = new ORM;
		return Object::$ORM->delete($obj);
	}
	
	public function save() {
		if($this->isNew) {
			$ret = Object::$ORM->add($this);
			if($ret)
				$this->isNew(false);
			return $ret;
		}
		else {
			return Object::$ORM->update($this);			
		}
	}
	
	public function getProperties() {
		$ret = array();
		$properties = (new \ReflectionClass($this))->getProperties(\ReflectionProperty::IS_PUBLIC);
		
		foreach($properties as $prop) {
			$prop_name = $prop->getName();
			if($prop_name != 'id') {
				if(preg_match('#@ref\((.*)\)#i',$prop->getDocComment(),$type))
					$ret[$prop_name] = $this->$prop_name->id;
				else {
					if(!is_numeric($this->$prop_name)) 
						$ret[$prop_name] = addslashes($this->$prop_name);						
					else
						$ret[$prop_name] = $this->$prop_name;
				}
			}
		}
		
		return $ret;
	}
}
