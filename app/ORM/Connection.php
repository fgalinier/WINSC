<?php

namespace Framework\ORM;

class Connection {
	public static $usr;
	public static $db;
	public static $host;
	private static $pwd;
	private static $PDO;
	private $query;

	public function __construct () {
		$data = json_decode(file_get_contents('config/database.json'),true);
		if($data == null or !is_array($data) or !isset($data['user']) or !isset($data['database']) 
			or !isset($data['host']) or !isset($data['password']))
			throw new \Exception();		
		Connection::$usr = $data['user'];
		Connection::$db = $data['database'];
		Connection::$host = $data['host'];
		Connection::$pwd = $data['password'];
		
		if(!Connection::$PDO)	Connection::$PDO = new \PDO("mysql:dbname=".Connection::$db.";host=".Connection::$host.";charset=UTF8",Connection::$usr,Connection::$pwd);
	}
	
	public function query($query) {
		return $this->query = $query;
	}
	
	public function select(array $cols = null) {
		$this->query = 'select ';
		if ($cols == null) $this->query .= '* ';
		else {
			$last = end($cols);
			foreach($cols as $col) {
				$this->query .= ('`'.$col.'`');
				$this->query .= ($col !== $last)?', ':' ';
			}
		}
		return $this;
	}
	
	public function count($col = '*', $distinct = false) {
		$this->query = 'select count('.($distinct?'distinct ':'').($col=='*'?'*':'`'.$col.'`').') ';
		return $this;
	}
	
	public function max($col) {
		$this->query = 'select max(`'.$col.'`) ';
		return $this;
	}
	
	public function from($obj) {
		if(strpos($this->query, 'select') === false) throw new \Exception;
		$this->query .= ('from `'.$obj.'` ');
		return $this;
	}
	
	public function where(array $conds = null) {
		if($conds == null) return $this;
		if(strpos($this->query, 'update') !== false) throw new \Exception;
		$this->query .= 'where ';
		$last = end($conds);
		foreach($conds as $cond) {
			$this->query .= $cond;
			$this->query .= ($cond !== $last)?' && ':' ';
		}
		return $this;
	}
	
	public function insert($obj, array $data) {
		$this->query = 'insert into `'.$obj.'` (';
		$keys = '';
		$values = '';
		$last = end(array_keys($data));
		foreach($data as $key => $value) {
			$keys .= ('`'.$key.'`');
			$values .= is_numeric($value)?$value:('"'.addslashes($value).'"');
			if($key !== $last) {
				$keys .= ',';
				$values .= ',';
			}
			$keys .= ' ';
			$values .= ' ';
		}
		$this->query .= ($keys.') values ('.$values.')');
		return $this;
	}
	
	public function update($obj,array $data,array $where) {
		$this->query = 'update `'.$obj.'` set ';
		$last = end(array_keys($data));
		foreach($data as $key => $value) {
			$this->query .= ('`'.$key.'` = '.(is_numeric($value)?$value:('"'.$value.'"')));
			if($key !== $last) {
				$this->query .= ',';
			}
			$this->query .= ' ';
		}
		$this->query .= 'where ';
		$last = end($where);
		foreach($where as $cond) {
			$this->query .= $cond;
			$this->query .= ($cond !== $last)?' && ':' ';
		}
		return $this;
	}
	
	public function delete($obj) {
		$this->query = 'delete from `'.$obj.'` ';
		return $this;
	}
	
	public function create($obj,array $properties,$options = null) {
		$this->query = 'create table IF NOT EXISTS `'.$obj.'` (';
		foreach($properties as $key => $value) {
			$this->query .= ('`'.$key.'` '.$value);
			$this->query .= ', ';
		}
		if($options) $this->query .= $options;
		$this->query .= 'primary key(id)) ENGINE=InnoDB DEFAULT CHARSET=latin1';
		return $this;
	}
	
	public function order(array $order = null, $desc = false) {
		if($order == null) return $this;
		if(strpos($this->query,'order by ') === false) $this->query .= 'order by ';
		$last = end($order);
		foreach($order as $value) {
			$this->query .= ('`'.$value.'`'.(($desc)?' desc':' asc'));
			if($value !== $last) {
				$this->query .= ',';
			}
			$this->query .= ' ';
		}
		return $this;
	}
	
	public function limit($limit = 1,$offset = 0) {
		if($limit == null) return $this;
		if($offset == null) $offset = 0;
		if(!is_integer($limit)) throw new \Exception;
		$this->query .= ('limit '.$limit.' offset '.$offset);
		return $this;
	}
	
	public function exec() {
		$this->query .= ';';
		if(strpos($this->query,'select') !== false) {
			return Connection::$PDO->query($this->query);
		}
		else {
			return Connection::$PDO->exec($this->query);
		}
	}
}


