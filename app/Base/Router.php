<?php

namespace Framework\Base;

class Router {
	private $routes;
	
	public function __construct() {
		$this->routes = json_decode(file_get_contents('config/routes.json'),true);
		if($this->routes == null or !is_array($this->routes))
			throw new \Exception();
	}
	
	public function findRoute($uri) {
		$initial_path = str_replace('router.php','',$_SERVER['SCRIPT_NAME']);
		preg_match('#'.$initial_path.'(.*)$#isU',$uri,$match); // Extract requested file
		$item = $match[1];
		$found = false;

		foreach ($this->routes as $value) {
			if(preg_match('#^'.$value['pattern'].'(\?(.*))?$#is',$item,$match)) {
				$found = true;
				$class = str_replace('/','\\',$value['controller']);
				$target = new $class;
				$get_i = array();
				
				if(!$target instanceof Controller) {
					echo \Framework\Template\View::generateError('<strong>Error:</strong> controller <em>'.$class.'</em> for route <em>'.$key.'</em> is not a valid Controller.',500);
					exit();
				}
				if(isset($value['action']))
					echo $target->$value['action']($match,$get_i);
				else
					echo $target->defaultAction($match,$get_i);
				
				break;
			}
		}
		
		return $found;
	}
	
	public function getPath($index) {
		return $this->routes[$index]['default'];
	}
}
