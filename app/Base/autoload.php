<?php
	
function __autoload($name) {
	$import;
	$class = str_replace('\\','/',$name);
	if(strpos($class,'Framework') === 0) {
		$import = 'app'.substr($class,9).'.php';
	}
	else {
		$import = 'src/'.$class.'.php';	
	}
	
	require_once($import);
}
