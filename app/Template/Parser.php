<?php

namespace Framework\Template;

class Parser {
	private $tokens;
	private $curr_tok;

	public function __construct($stream) {
		$this->tokens = Parser::lexicalParser($stream);
		$this->curr_tok = 0;
	}
	
	private static function lexicalParser($stream) {
		$i = 0;
		$tok = array();
		foreach(preg_split('#(<%[\s\S]*%>)#iU',$stream,-1,PREG_SPLIT_DELIM_CAPTURE) as $capt) {
			if(preg_match('#<%([\s\S]*)%>#iU',$capt,$matches)) {
				if(preg_match('#<%\s*foreach\s*([\S]*)\s*in\s*([\S]*)\s*%>#iU',$capt,$m)) {
					$tok[] = array('FOREACH',trim($m[1]),trim($m[2]));
					continue;
				}
				if(preg_match('#<%\s*endfor\s*%>#iU',$capt)) {
					$tok[] = array('ENDFOR');
					continue;
				}
				if(preg_match('#<%\s*if\s*([\s\S]*)\s*%>#iU',$capt,$m)) {
					$tok[] = array('IF',trim($m[1]));
					continue;
				}
				if(preg_match('#<%\s*else\s*%>#iU',$capt)) {
					$tok[] = array('ELSE');
					continue;
				}
				if(preg_match('#<%\s*endif\s*%>#iU',$capt)) {
					$tok[] = array('ENDIF');
					continue;
				}
				if(preg_match('#<%\s*(\S*)\((\S*)\)\s*%>#iU',$capt,$m)) {
					$tok[] = array('CALL',trim($m[1]),trim($m[2]));
					continue;
				}
				if(preg_match('#<%\s*(\S*)\.(\S*)\s*%>#iU',$capt,$m)) {
					$tok[] = array('INDARG',trim($m[1]),trim($m[2]));
					continue;
				}
				if(preg_match('#<%\s*(\S*)\s*%>#iU',$capt,$m)) {
					$tok[] = array('ARG',trim($m[1]));
					continue;
				}
			}
			else {
				$tok[] = array('HTML',$capt);
			}
		}
		return $tok;
	}
	
	public function parse() {
		$tree = array();
		while($this->curr_tok < count($this->tokens)) {
			$t = $this->tokens[$this->curr_tok++];
			switch($t[0]) {
				case 'IF':
					$tmp = array($t,$this->parse());
					if($this->tokens[$this->curr_tok-1][0] == 'ELSE') {
						$tmp[] = $this->parse();
					}
					$tree[] = $tmp;
					break;
				case 'FOREACH':
					$tree[] = array($t,$this->parse());			
					break;
				case 'ELSE':
					return $tree;
				case 'ENDIF':
				case 'ENDFOR':
					return $tree;			
					break;
				default:
					$tree[] = $t;
					
			}
		}
		return $tree;
	}
	
	public function parseIf() {
		$tree = array();
		$pos = 0;
		while($this->curr_tok < count($this->tokens)) {
			$t = $this->tokens[$this->curr_tok++];
			switch($t[0]) {
				case 'ELSE':
					$tree[$pos][] = $t;					
					$pos++;
					break;
				case 'ENDIF':
					return $tree;
				default:
					$tree[$pos][] = $this->parse($t);
					
			}
		}
		return $tree;		
	}
	
}


