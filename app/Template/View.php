<?php

namespace Framework\Template;

class View {
	public static function generate($path, $args) {
		header("Status: 200 OK", true, 200);
		return View::parseFile(file_get_contents($path),$args);
	}
	
	public static function generateError($message, $code) {
		if($code == 404)
			header("Status: 404 Not Found", true, 404);
		else
			header("Status: 500 Internal Server Error", true, 500);
			
		return View::parseFile(file_get_contents('resources/errors/'.$code.'.php'),array('code' => $code));
	}
	
	private static function parseFile($string, $args) {
		$ret = preg_replace_callback('#<%\s*include\s*(\S*)\s*%>#i', 
		function ($matches) use ($args) {
			return View::parseFile(file_get_contents(trim($matches[1])),$args);
		}, 
		$string);
		$p = new Parser($ret);
		return View::interpret($p->parse(), $args);
	}
	
	private static function interpret($tree, array $args) {
		$content = '';
		foreach($tree as $leaf) {
			if(is_array($leaf[0])) {
				$instr = $leaf[0];
				switch($instr[0]) {
					case 'IF':
						$not = false;
						$test = $instr[1];
						if($not = preg_match('#not\s*(\S*)#i',$test,$matches)) {
							$test = $matches[1];
						}
						if(preg_match('#(\S*)\((\S*)\)#i',$test,$matches)) {
							$test = View::$matches[1]($matches[2],$args);
						}
						else {
							$test = $args[$test];
						}
						
						if(($not)?!$test:$test) {
							$content .= View::interpret($leaf[1], $args);
						}
						else {
							if(isset($leaf[2])) {
								$content .= View::interpret($leaf[2], $args);							
							}
						}
						break;
					case 'FOREACH':
						if(isset($args[$instr[2]])) {
							foreach($args[$instr[2]] as $tmp) {
								$content .= View::interpret($leaf[1], $args+array($instr[1] => $tmp));							
							}
						}
						break;
				}
			}
			else {
				switch($leaf[0]) {
					case 'CALL':
						$content .= View::$leaf[1]($leaf[2],$args);
						break;
					case 'INDARG':
						$content .= stripslashes($args[$leaf[1]]->$leaf[2]);
						break;
					case 'ARG':
						$content .= stripslashes($args[$leaf[1]]);
						break;
					case 'HTML':
						$content .= $leaf[1];
						break;
				}
			}
		}
		return $content;
	}
	
	private static function path($arg) {
		$initial_path = str_replace('router.php','',$_SERVER['SCRIPT_NAME']);
	 	$router = new \Framework\Base\Router;
	 	return $initial_path.$router->getPath(trim($arg));	
	}
	
	private static function gentime() {
	 	return sprintf("%.4f",microtime(true)-$_SERVER["REQUEST_TIME_FLOAT"]);
	}
	
	private static function link($link) {
		$initial_path = str_replace('router.php','',$_SERVER['SCRIPT_NAME']);
		return $initial_path.$link;
	}
	
	private static function exists($var,$args) {
		return isset($args[$var]) || isset($_SESSION[$var]);
	}
	
	private static function equals($var,$args) {
		$param = explode(',',$var);
		$eq = true;
		foreach($param as $a) {
			foreach($param as $b) {
				$eq = $eq && (isset($args[$a])?$args[$a]:$a) == (isset($args[$b])?$args[$b]:$b);
			}
		}
		
		return $eq;
	}
}


