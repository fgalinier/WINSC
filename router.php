<?php

session_start();

require_once('app/Base/autoload.php');

$uri = '';

new Framework\ORM\Connection;

if (isset($_SERVER['REQUEST_URI'])) {
	$uri = trim($_SERVER['REQUEST_URI']);
}

try {
	$router = new Framework\Base\Router;
}
catch (Exception $e) {
	echo Framework\Template\View::generateError('<strong>Error:</strong> routes file is not a valid json file.',500);
	exit();
}

if(!$router->findRoute($uri)) {
	echo Framework\Template\View::generateError('',404);	
	exit();
}
