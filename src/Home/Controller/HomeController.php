<?php

namespace Home\Controller;
use Framework\Base\Controller;;
use Framework\Template\View;
use Home\Object\Article;

class HomeController extends Controller {
	public function indexAction() {
		return View::generate('src/Home/resources/views/index.html',array());
	}
}

